// this file handles all the routes for the todolist app
// usually it should be inside a folder called routes, but for the sake of simplicity, I'm keeping it in the root folder
import express from "express";
import { Status } from "./types";
import TodolistController from "./todolistController";
import TodolistPersistance from "./todoPersistance";

const router = express.Router();

const todolistPersistance = new TodolistPersistance();

// thought of using a class component to organize all the todolist related functions in a one place
const todolistController = new TodolistController({
  todolistPersistance,
});

// server.ts file has the base path as /todos. complete route path is <server_host>/todos/
router.get("/", (_req, res) => {
  const todos = todolistController.getAllTodos();
  res.status(200).json(todos);
});

router.post("/", (req, res) => {
  try {
    const { title, status }: { title: string; status: Status } = req.body;

    if (!title || !Object.values(Status).includes(status)) {
      res.status(400).send({ message: "Invalid request body" });
    }

    const todo = todolistController.addNewTodo(title, status);

    res.status(201).json(todo);
  } catch (e) {
    res.status(400).json({
      error: e,
    });
  }
});

router.delete("/:id", (req, res) => {
  try {
    const { id } = req.params;

    if (!id) {
      res.status(400).send({ error: "Request Id is missing" });
    }

    const todo = todolistController.deleteTodo(parseInt(id));

    if (!todo) {
      // using 404 to show that the todo is not found
      return res.status(404).json({ error: "Todo not found" });
    }

    // sometimes it is not wise to return deleted data as they might contain sensitive information
    // in this case, it is fine to return the deleted todo and that can be helpful for the frontend
    res.json(todo);
  } catch (e) {
    res.status(400).json({
      error: e,
    });
  }
});

router.patch("/:id", (req, res) => {
  const { id } = req.params;
  const { status }: { status: Status } = req.body;

  if (!Object.values(Status).includes(status)) {
    return res.status(400).json({ error: "Invalid status type" });
  }

  const todo = todolistController.updateTodo(parseInt(id), status);

  if (!todo) {
    return res.status(404).json({ error: "Todo not found" });
  }

  res.json(todo);
});

export default router;
