import {
  ItodolistPersistance,
  ItodolistController,
  Status,
  Todo,
  TodolistAppDependencies,
} from "./types";

// TodolistController class implements ItodolistController interface which acts as a contract for the class
class TodolistController implements ItodolistController {
  private todoPersistance: ItodolistPersistance;

  constructor(dependencies: TodolistAppDependencies) {
    // I could have instantiate the TodolistPersistance class here.
    // But I'm passing it as a dependency to make it more testable
    this.todoPersistance = dependencies.todolistPersistance;
  }

  addNewTodo = (title: string, status: Status): Todo => {
    return this.todoPersistance.addNewTodo(title, status);
  };

  getAllTodos(): Todo[] {
    return this.todoPersistance.getAllTodos();
  }

  updateTodo = (id: number, status: Status): Todo | undefined => {
    return this.todoPersistance.updateTodo(id, status);
  };

  deleteTodo = (id: number): Todo | undefined => {
    return this.todoPersistance.deleteTodoById(id);
  };
}

export default TodolistController;
