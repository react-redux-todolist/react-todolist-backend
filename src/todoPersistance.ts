import { ItodolistPersistance, Status, Todo } from "./types";

class TodolistPersistance implements ItodolistPersistance {
  // Initial data of the appp
  private todos: Todo[] = [
    { id: 1, title: "Create todo list", status: Status.Completed },
  ];

  private nextId = 2;

  private getNextId = (): number => {
    return this.nextId++;
  };

  private getTodoById = (id: number): Todo | undefined => {
    return this.todos.find((t) => t.id === id);
  };

  addNewTodo = (title: string, status: Status): Todo => {
    const newTodo = {
      id: this.getNextId(),
      title: title,
      status: status,
    };

    this.todos.push(newTodo);
    return newTodo;
  };

  getAllTodos = (): Todo[] => {
    return this.todos;
  };

  updateTodo = (id: number, status: Status): Todo | undefined => {
    const todo = this.getTodoById(id);

    if (!todo) {
      return undefined;
    }

    todo.status = status;
    return todo;
  };

  deleteTodoById = (id: number): Todo | undefined => {
    const todoIndex = this.todos.findIndex((t) => t.id === id);

    if (todoIndex === -1) {
      return undefined;
    }

    const todo = this.todos[todoIndex];
    this.todos.splice(todoIndex, 1);
    return todo;
  };
}

export default TodolistPersistance;
