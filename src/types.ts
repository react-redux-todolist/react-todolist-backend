// this file contain all the types that are used in the project.
export enum Status {
  Completed = "completed",
  Pending = "pending",
}

export type Todo = {
  id: number;
  title: string;
  status: Status;
};

export interface ItodolistController {
  getAllTodos(): Todo[];
  addNewTodo: (title: string, status: Status) => Todo;
  updateTodo: (id: number, status: Status) => Todo | undefined;
  deleteTodo: (id: number) => Todo | undefined;
}

export interface ItodolistPersistance {
  getAllTodos(): Todo[];
  addNewTodo: (title: string, status: Status) => Todo;
  updateTodo: (id: number, status: Status) => Todo | undefined;
  deleteTodoById: (id: number) => Todo | undefined;
}

export type TodolistAppDependencies = {
  todolistPersistance: ItodolistPersistance;
};
