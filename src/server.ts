import * as dotenv from "dotenv";
import express from "express";
import cors from "cors";
import helmet from "helmet";

import routes from "./routes";

dotenv.config();

const app = express();

// applying express middlewares
app.use(helmet()); // enhances the security of the app
app.use(cors()); // This is CORS-enabled for all origins!
app.use(express.json()); // parses the incoming request body into JSON

// using this jsut to showcase how to use next function for middleware chaining
// we should use other logging mechanisms like datadog, winston, etc. for production
app.use((req, res, next) => {
  let now = new Date(Date.now());
  console.debug(`${now.toLocaleString()}, ${req.method}, ${req.path}`);
  next();
});

// Handling todos routes
app.use("/todos", routes);

// fallback route
app.use("*", (_req, res) => {
  res.status(404).json({
    error: "Invalid path",
  });
});

export default app;
