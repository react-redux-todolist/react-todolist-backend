import TodolistController from "../todolistController";
import { ItodolistPersistance, Status } from "../types";

const mockedTodolistPersistance: ItodolistPersistance = {
  addNewTodo: jest.fn(),
  getAllTodos: jest.fn(),
  updateTodo: jest.fn(),
  deleteTodoById: jest.fn(),
};

describe("TodolistController", () => {
  let todolistController = new TodolistController({
    todolistPersistance: mockedTodolistPersistance,
  });

  it("should return all todos", () => {
    todolistController.getAllTodos();
    expect(mockedTodolistPersistance.getAllTodos).toHaveBeenCalledTimes(1);
    expect(mockedTodolistPersistance.getAllTodos).toHaveBeenCalledWith(); // with no arguments
  });

  it("should add a new todo", () => {
    todolistController.addNewTodo("title", Status.Completed);
    expect(mockedTodolistPersistance.addNewTodo).toHaveBeenCalledTimes(1);
    expect(mockedTodolistPersistance.addNewTodo).toHaveBeenCalledWith(
      "title",
      Status.Completed
    );
  });

  it("should update a todo", () => {
    todolistController.updateTodo(1, Status.Completed);
    expect(mockedTodolistPersistance.updateTodo).toHaveBeenCalledTimes(1);
    expect(mockedTodolistPersistance.updateTodo).toHaveBeenCalledWith(
      1,
      Status.Completed
    );
  });

  it("should delete a todo", () => {
    todolistController.deleteTodo(1);
    expect(mockedTodolistPersistance.deleteTodoById).toHaveBeenCalledTimes(1);
    expect(mockedTodolistPersistance.deleteTodoById).toHaveBeenCalledWith(1);
  });
});
