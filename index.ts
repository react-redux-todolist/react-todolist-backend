import "dotenv/config";
import app from "./src/server";

// setting a fallback value for port in case no environment variables were provided.
const PORT = process.env.PORT || 5000;

app.listen(PORT, async () => {
  console.log(`listning on port ${PORT}`);
});
