# My simple Todolist REST API.

# Getting started

## Prerequisites

Make sure you have the following installed on your machine before you begin:

- **Node.js**
- **Yarn** ( your favourite node package manager should work )

this project is tested on node **v20.11.0** and yarn **v1.22.21**

First navigate to the project root and run these two commands on the terminal:

```bash
$ yarn install
$ yarn start
```

Unit tests for todolistController.ts is also available.
use following command to invoke tests

you may use --coverage flag with following command to view test coverage

```bash
$ yarn test
```

## features

- list all todos.
- add a new todo.
- delete a todo
- update todo status
